import React from "react";
import ReactDOM from "react-dom";
import faker from "faker";
import CommentDetail from "./CommentDetail";
import ApprovalCard from "./ApprovalCard";

const App = () => {
  return (
    <div className="ui container comments">
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName() + " " + faker.name.lastName()}
          timeAgo="Today at 4:25PM"
          avatar={faker.image.avatar()}
          commentText={faker.lorem.sentences()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName() + " " + faker.name.lastName()}
          timeAgo="Today at 1:01AM"
          avatar={faker.image.avatar()}
          commentText={faker.lorem.sentences()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName() + " " + faker.name.lastName()}
          timeAgo="Yesterday at 8:44PM"
          avatar={faker.image.avatar()}
          commentText={faker.lorem.sentences()}
        />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail
          author={faker.name.firstName() + " " + faker.name.lastName()}
          timeAgo="Yesterday at 5:19AM"
          avatar={faker.image.avatar()}
          commentText={faker.lorem.sentences()}
        />
      </ApprovalCard>
    </div>
  );
};

ReactDOM.render(<App />, document.querySelector("#root"));
